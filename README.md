Ejercicios sobre XML
===================
Esta aplicación se compone de 4 programas, el primero para hacer medias de datos en XML, el segundo para visualizar la predicción del tiempo de Málaga, la tercera para ver el estado de las estaciones de bici de Zaragoza y el último para ver las noticias de 4 distintos periódicos.

![Screenshot_1481478217.png](https://bitbucket.org/repo/gdaBjr/images/4167584917-Screenshot_1481478217.png)

1. Medias
---------------------

![Screenshot_1481478222.png](https://bitbucket.org/repo/gdaBjr/images/2546410134-Screenshot_1481478222.png)

Coge un fichero XML guardado en el teléfono con esta composición:

<?xml version="1.0" encoding="utf-8"?>

<empleados>

    <empleado>

        <nombre>Romualdo</nombre>

        <puesto>Secretario</puesto>

        <edad>36</edad>

        <sueldo>1300</sueldo>

    </empleado>

    <empleado>

        <nombre>Rosa</nombre>

        <puesto>Oficinista</puesto>

        <edad>29</edad>

        <sueldo>12000</sueldo>

    </empleado>

    <empleado>

        <nombre>Salsadote</nombre>

        <puesto>Oficinista</puesto>

        <edad>38</edad>

        <sueldo>1200</sueldo>

    </empleado>

    <empleado>

        <nombre>Suaso</nombre>

        <puesto>Jefe</puesto>

        <edad>89</edad>

        <sueldo>13000</sueldo>

    </empleado>

    <empleado>

        <nombre>Sans</nombre>

        <puesto>Informatico</puesto>

        <edad>36</edad>

        <sueldo>1500</sueldo>

    </empleado>

</empleados>

Y hace la media de la edad de los empleados y muestra El sueldo mínimo y el sueldo máximo de éstos.

2. El tiempo
------------------

![Screenshot_1481478233.png](https://bitbucket.org/repo/gdaBjr/images/992950356-Screenshot_1481478233.png)

Coge el tiempo en un archivo XML proporcionado por una página web y guarda su predicción y la muestra, los iconos vienen en el fichero y cuando se crea la tabla donde se muestra se descargan.
La predicción que se muestra es la de hoy y mañana del día en el que se ejecute.

3. Estaciones de bici
---------------------

![Screenshot_1481478248.png](https://bitbucket.org/repo/gdaBjr/images/441853097-Screenshot_1481478248.png)

Es una lista de las estaciones de bicicletas de Zaragoza actualizada, los datos se descargan del xml http://www.zaragoza.es/api/recurso/urbanismo-infraestructuras/estacion-bicicleta.xml; que muestra:
- Un icono proporcionado por la web.
- El ID de la estación bajo el icono.
- La dirección/nombre de la estación.
- El estado (Si está abierto o no).
- La cantidad de bicicletas disponible.
- Los anclajes disponibles.

También tiene un botón flotante para refrescar la lista.

Al pulsar en la ficha de la estación deseada se abre el mapa de Zaragoza

4. Noticias RSS
------------------

![Screenshot_1481478257.png](https://bitbucket.org/repo/gdaBjr/images/1514438741-Screenshot_1481478257.png)

Lo primero que se ven son 4 botones para ver las diferentes plataformas de noticias: Pc World, El Mundo Today, El País y Linux Magacine.
Al pulsar en cualquiera de estos se ve un listado con un título personalizado por cada botón, en el listado en sí se ven todas las noticias actualizadas de cada plataforma.

![Screenshot_1481478263.png](https://bitbucket.org/repo/gdaBjr/images/3515078893-Screenshot_1481478263.png)

![Screenshot_1481478269.png](https://bitbucket.org/repo/gdaBjr/images/3363249641-Screenshot_1481478269.png)