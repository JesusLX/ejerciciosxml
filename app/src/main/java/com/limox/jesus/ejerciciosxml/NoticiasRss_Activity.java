package com.limox.jesus.ejerciciosxml;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.limox.jesus.ejerciciosxml.Adapters.NoticiasAdapter;
import com.limox.jesus.ejerciciosxml.Pojo.Noticia;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;

import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class NoticiasRss_Activity extends AppCompatActivity {

    public static final String TEMPORAL = "elmundotdy.xml";
    ArrayList<Noticia> noticias = new ArrayList<>();
    NoticiasAdapter adapter;
    ListView lvNoticias;
    TextView txvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_noticias_rss);
        lvNoticias = (ListView) findViewById(R.id.nr_lvNoticias);
        txvTitle = (TextView) findViewById(R.id.nr_txvTitle);
        Intent intent = getIntent();
        descarga(intent.getStringExtra("rss"),TEMPORAL);
        txvTitle.setText(intent.getStringExtra("title"));
    }

    private void descarga(String rss, String temporal) {
        final ProgressDialog progreso = new ProgressDialog(this);
        File miFichero = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), temporal);
        try {
            if (miFichero.exists()){
                miFichero.createNewFile();
            }
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this,e.getMessage(),Toast.LENGTH_LONG).show();
        }
        RestClient.get(rss, new FileAsyncHttpResponseHandler(miFichero) {
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                progreso.dismiss();
                Log.e("dismiss",throwable.getMessage());
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                try {
                    noticias = new ArrayList<Noticia>(Analisis.analizarRSS(file));
                    adapter = new NoticiasAdapter(NoticiasRss_Activity.this, noticias);
                    lvNoticias.setAdapter(adapter);

                } catch (XmlPullParserException e) {
                    Toast.makeText(NoticiasRss_Activity.this,"Excepción XML:"+e.getMessage(),Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                progreso.dismiss();
            }

            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Conectando . . .");
                progreso.setCancelable(false);
                progreso.show();
            }

        });
    }
}
