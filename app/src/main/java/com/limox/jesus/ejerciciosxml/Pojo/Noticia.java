package com.limox.jesus.ejerciciosxml.Pojo;

/**
 * Created by jesus on 7/12/16.
 */

public class Noticia {

    private String mTitle;
    private String mUrl;

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String mUrl) {
        this.mUrl = mUrl;
    }
}
