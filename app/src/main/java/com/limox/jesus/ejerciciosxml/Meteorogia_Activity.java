package com.limox.jesus.ejerciciosxml;

import android.app.ProgressDialog;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.limox.jesus.ejerciciosxml.Adapters.EstacionesAdapter;
import com.limox.jesus.ejerciciosxml.Pojo.Estacion;
import com.limox.jesus.ejerciciosxml.Pojo.Tiempo;
import com.loopj.android.http.FileAsyncHttpResponseHandler;

import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import it.sephiroth.android.library.picasso.Picasso;

public class Meteorogia_Activity extends AppCompatActivity {

    TextView l1_TxvDate,l2_TxvDate,l1_TxvTemperatura,l2_TxvTemperatura,l1_TxvPrediction,l2_TxvPrediction;
    ImageView l1_IvIcon,l2_IvIcon;

    private static final String WEB = "http://xml.tutiempo.net/xml/3589.xml";
    private static final String TEMPORAL = "tiemporal.xml";

    ArrayList<Tiempo> tiempos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meteorogia);
        l1_TxvDate = (TextView) findViewById(R.id.m_l1_TxvDate);
        l2_TxvDate = (TextView) findViewById(R.id.m_l2_TxvDate);
        l1_TxvTemperatura = (TextView) findViewById(R.id.m_l1_TxvTemperatura);
        l2_TxvTemperatura = (TextView) findViewById(R.id.m_l2_TxvTemperatura);
        l1_TxvPrediction = (TextView) findViewById(R.id.m_l1_TxvTexto);
        l2_TxvPrediction = (TextView) findViewById(R.id.m_l2_TxvTexto);
        l1_IvIcon = (ImageView) findViewById(R.id.m_l1_IvIcon);
        l2_IvIcon = (ImageView) findViewById(R.id.m_l2_IvIcon);

        descarga(WEB,TEMPORAL);
    }

    private void descarga(String web, String temporal) {
        final ProgressDialog progreso = new ProgressDialog(this);
        File miFichero = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), temporal);
        try {
            miFichero.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        RestClient.get(web, new FileAsyncHttpResponseHandler(miFichero) {
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                progreso.dismiss();
                Log.e("dismiss",throwable.getMessage());
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                try {
                    tiempos = new ArrayList<Tiempo>(Analisis.analizarTiempo(file));

                    fill();

                } catch (XmlPullParserException e) {
                    Toast.makeText(Meteorogia_Activity.this,"Excepción XML:"+e.getMessage(),Toast.LENGTH_SHORT);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                progreso.dismiss();
            }

            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Conectando . . .");
                progreso.setCancelable(false);
                progreso.show();
            }

        });

    }
    private void fill(){
        l1_TxvDate.setText(tiempos.get(0).getDate());
        l2_TxvDate.setText(tiempos.get(1).getDate());
        l1_TxvTemperatura.setText(tiempos.get(0).getTempMax()+"/"+tiempos.get(0).getTempMin());
        l2_TxvTemperatura.setText(tiempos.get(1).getTempMax()+"/"+tiempos.get(1).getTempMin());
        try {
            Picasso.with(Meteorogia_Activity.this).load(tiempos.get(0).getIcon()).error(R.mipmap.ic_launcher).into(l1_IvIcon);
            Picasso.with(Meteorogia_Activity.this).load(tiempos.get(1).getIcon()).error(R.mipmap.ic_launcher).into(l2_IvIcon);
        } catch (Exception e) {
        }
        l1_TxvPrediction.setText(tiempos.get(0).getTexto());
        l2_TxvPrediction.setText(tiempos.get(1).getTexto());
    }
}
