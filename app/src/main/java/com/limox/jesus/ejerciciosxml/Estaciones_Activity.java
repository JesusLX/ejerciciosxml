package com.limox.jesus.ejerciciosxml;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.limox.jesus.ejerciciosxml.Adapters.EstacionesAdapter;
import com.limox.jesus.ejerciciosxml.Pojo.Estacion;
import com.loopj.android.http.FileAsyncHttpResponseHandler;

import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

import static com.loopj.android.http.AsyncHttpClient.log;

public class Estaciones_Activity extends AppCompatActivity {

    ArrayList<Estacion> estaciones;
    EstacionesAdapter adapter;
    ListView lvEstaciones;
    FloatingActionButton fabtnReflesh;

    private static final String WEB = "http://www.zaragoza.es/api/recurso/urbanismo-infraestructuras/estacion-bicicleta.xml";
    private static final String TEMPORAL = "temporal.xml";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estaciones);
        lvEstaciones = (ListView) findViewById(R.id.e_lvEstaciones);
        fabtnReflesh = (FloatingActionButton) findViewById(R.id.e_fabtnRefresh);
        descarga(WEB,TEMPORAL);
        fabtnReflesh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                descarga(WEB,TEMPORAL);
            }
        });

        lvEstaciones.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                log.d("coordenadas",estaciones.get(i).getCordenadas());
               // Uri uri= Uri.parse("geo:"+estaciones.get(i).getCordenadas()+"(Zaragoza)");
                Uri uri = Uri.parse("http://www.zaragoza.es/ciudad/viapublica/movilidad/bici/carril/ver_Mapa?id=2");
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(uri);
                startActivity(intent);
            }
        });
    }




    private void descarga(String web, String temporal) {
        final ProgressDialog progreso = new ProgressDialog(this);
        File miFichero = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), temporal);
        try {
            miFichero.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        RestClient.get(web, new FileAsyncHttpResponseHandler(miFichero) {
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                progreso.dismiss();
                Log.e("dismiss",throwable.getMessage());
                Toast.makeText(Estaciones_Activity.this,"Excepción XML:"+throwable.getMessage(),Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                try {
                    estaciones = new ArrayList<Estacion>(Analisis.analizarEstaciones(file));
                    adapter = new EstacionesAdapter(Estaciones_Activity.this, estaciones);
                    lvEstaciones.setAdapter(adapter);

                } catch (XmlPullParserException e) {
                    Toast.makeText(Estaciones_Activity.this,"Excepción XML:"+e.getMessage(),Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                progreso.dismiss();
            }

            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Conectando . . .");
                progreso.setCancelable(false);
                progreso.show();
            }

        });
    }
}
