package com.limox.jesus.ejerciciosxml.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.limox.jesus.ejerciciosxml.Estaciones_Activity;
import com.limox.jesus.ejerciciosxml.Pojo.Estacion;
import com.limox.jesus.ejerciciosxml.R;

import java.util.ArrayList;

import it.sephiroth.android.library.picasso.Picasso;

/**
 * Created by jesus on 7/12/16.
 */

public class EstacionesAdapter extends ArrayAdapter<Estacion> {

    public EstacionesAdapter(Context context, ArrayList<Estacion> estaciones) {
        super(context, R.layout.item_estaciones, estaciones);

    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        StationsHolder stationsHolder = null;

        if (view == null){
            LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(getContext().LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.item_estaciones,null);

            stationsHolder = new StationsHolder();
            stationsHolder.ivIco = (ImageView) view.findViewById(R.id.ie_ivIcon);
            stationsHolder.txvTitle = (TextView) view.findViewById(R.id.ie_txvTitle);
            stationsHolder.txvId = (TextView) view.findViewById(R.id.ie_txvId);
            stationsHolder.txvEstado = (TextView) view.findViewById(R.id.ie_txvEstado);
            stationsHolder.txvBicis = (TextView) view.findViewById(R.id.ie_txvBicisDisp);
            stationsHolder.txvAnclajes = (TextView) view.findViewById(R.id.ie_txvAnclajesDisp);

            view.setTag(stationsHolder);
        }else
            stationsHolder = (StationsHolder) view.getTag();

        try {
            Picasso.with(getContext()).load(getItem(position).getIcon()).error(R.mipmap.ic_launcher).into(stationsHolder.ivIco);
        } catch (Exception e) {
        }
        stationsHolder.txvTitle.setText(getItem(position).getName());
        stationsHolder.txvId.setText(getItem(position).getId());
        stationsHolder.txvEstado.setText(getItem(position).getEstado());
        stationsHolder.txvBicis.setText(Integer.toString(getItem(position).getBicisDisponibles()));
        stationsHolder.txvAnclajes.setText(Integer.toString(getItem(position).getAnclajesDisponibles()));




        return view;
    }
    class StationsHolder {
        ImageView ivIco;
        TextView txvId;
        TextView txvTitle;
        TextView txvEstado;
        TextView txvBicis;
        TextView txvAnclajes;
    }
}
