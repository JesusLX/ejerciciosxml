package com.limox.jesus.ejerciciosxml;

import android.content.Context;
import android.content.res.XmlResourceParser;
import android.net.Uri;
import android.util.Xml;

import com.limox.jesus.ejerciciosxml.Pojo.Estacion;
import com.limox.jesus.ejerciciosxml.Pojo.Noticia;
import com.limox.jesus.ejerciciosxml.Pojo.Tiempo;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by usuario on 21/11/16.
 */

public class Analisis {
    public static String analizar(String texto) throws XmlPullParserException, IOException {
        StringBuilder cadena = new StringBuilder();
        XmlPullParser xpp = Xml.newPullParser();
        xpp.setInput(new StringReader(texto));
        int eventType = xpp.getEventType();
        cadena.append("Inicio . . . \n");
        while (eventType != XmlPullParser.END_DOCUMENT) {
            if (eventType == XmlPullParser.START_DOCUMENT) {
                //System.out.println("Start document");
                cadena.append("Start document" + "\n");
            } else if (eventType == XmlPullParser.START_TAG) {
                //stem.out.println("Start tag "+xpp.getName());
                cadena.append("Start tag " + xpp.getName() + "\n");
            } else if (eventType == XmlPullParser.END_TAG) {
                //System.out.println("End tag "+xpp.getName());
                cadena.append("End tag " + xpp.getName() + "\n");
            } else if (eventType == XmlPullParser.TEXT) {
                //System.out.println("Text "+xpp.getText());
                cadena.append("Text " + xpp.getText() + "\n");
            }
            eventType = xpp.next();
        }
        //System.out.println("End document");
        cadena.append("End document" + "\n" + "Fin");
        return cadena.toString();
    }

    public static String[] analizarNombres(Context c) throws XmlPullParserException, IOException {

        boolean esEdad = false;
        boolean esSueldo = false;
        int sumaEdades = 0;
        double mayorSueldo = 0;
        double menorSueldo = -1;
        double tmpSueldo = 0;
        int contador = 0;
        String[] resultados = new String[3];
        XmlResourceParser xrp = c.getResources().getXml(R.xml.empleados);
        int eventType = xrp.getEventType();
        while (eventType != XmlPullParser.END_DOCUMENT) {
            switch (eventType) {
                case XmlPullParser.START_TAG:
                    if (xrp.getName().equals("edad"))
                        esEdad = true;
                    if (xrp.getName().equals("sueldo"))
                        esSueldo = true;

                    break;
                case XmlPullParser.TEXT:
                    if (esEdad) {
                        sumaEdades += Integer.parseInt(xrp.getText());
                        contador++;
                    }
                    if (esSueldo) {
                        tmpSueldo = Double.parseDouble(xrp.getText());
                        if (menorSueldo == -1)
                            menorSueldo = tmpSueldo;
                        if (tmpSueldo > mayorSueldo)
                            mayorSueldo = tmpSueldo;
                        if (tmpSueldo < menorSueldo)
                            menorSueldo = tmpSueldo;
                    }
                    break;
                case XmlPullParser.END_TAG:
                    if (xrp.getName().equals("edad"))
                        esEdad = false;
                    if (xrp.getName().equals("sueldo"))
                        esSueldo = false;
                    break;
            }
            eventType = xrp.next();
        }
        resultados[0] = Integer.toString(sumaEdades / contador);
        resultados[1] = Double.toString(mayorSueldo);
        resultados[2] = Double.toString(menorSueldo);
        return resultados;
    }

    public static ArrayList<Estacion> analizarEstaciones(File file) throws XmlPullParserException, IOException {

        ArrayList<Estacion> estaciones = new ArrayList<>();
        Estacion tmpEstacion = null;
        boolean dentroEstacion = false;
        boolean dentroId = false;
        boolean dentroName = false;
        boolean dentroEstado = false;
        boolean dentroBicisDisponibles = false;
        boolean dentroAnclajesDisponibles = false;
        boolean dentroUri = false;
        boolean dentroIcon = false;
        boolean dentroCoordenadas = false;

        XmlPullParser xpp = Xml.newPullParser();
        xpp.setInput(new FileReader(file));
        int eventType = xpp.getEventType();

        while (eventType != XmlPullParser.END_DOCUMENT) {
            switch (eventType) {
                case XmlPullParser.START_TAG:
                    if (xpp.getName().equals("estacion")) {
                        dentroEstacion = true;
                        tmpEstacion = new Estacion();
                    }
                    if (xpp.getName().equals("id"))
                        dentroId = true;
                    if (xpp.getName().equals("uri"))
                        dentroUri = true;
                    if (xpp.getName().equals("title"))
                        dentroName = true;
                    if (xpp.getName().equals("estado"))
                        dentroEstado = true;
                    if (xpp.getName().equals("bicisDisponibles"))
                        dentroBicisDisponibles = true;
                    if (xpp.getName().equals("anclajesDisponibles"))
                        dentroAnclajesDisponibles = true;
                    if (xpp.getName().equals("icon"))
                        dentroIcon = true;
                    if (xpp.getName().equals("coordinates"))
                        dentroCoordenadas = true;
                    break;
                case XmlPullParser.TEXT:
                    if (dentroId)
                        tmpEstacion.setId(xpp.getText());
                    if (dentroUri)
                        tmpEstacion.setLink(xpp.getText());
                    if (dentroName)
                        tmpEstacion.setName(xpp.getText());
                    if (dentroEstado)
                        tmpEstacion.setEstado(xpp.getText());
                    if (dentroBicisDisponibles)
                        tmpEstacion.setBicisDisponibles(Integer.parseInt(xpp.getText()));
                    if (dentroAnclajesDisponibles)
                        tmpEstacion.setAnclajesDisponibles(Integer.parseInt(xpp.getText()));
                    if (dentroIcon)
                        tmpEstacion.setIcon(xpp.getText());
                    if (dentroCoordenadas)
                        tmpEstacion.setCordenadas(xpp.getText());
                    break;
                case XmlPullParser.END_TAG:
                    if (xpp.getName().equals("estacion")) {
                        dentroEstacion = false;
                        estaciones.add(tmpEstacion);
                    }
                    if (xpp.getName().equals("id") && dentroEstacion)
                        dentroId = false;
                    if (xpp.getName().equals("uri") && dentroEstacion)
                        dentroUri = false;
                    if (xpp.getName().equals("title") && dentroEstacion)
                        dentroName = false;
                    if (xpp.getName().equals("estado") && dentroEstacion)
                        dentroEstado = false;
                    if (xpp.getName().equals("bicisDisponibles") && dentroEstacion)
                        dentroBicisDisponibles = false;
                    if (xpp.getName().equals("anclajesDisponibles") && dentroEstacion)
                        dentroAnclajesDisponibles = false;
                    if (xpp.getName().equals("icon") && dentroEstacion)
                        dentroIcon = false;
                    if (xpp.getName().equals("coordinates") && dentroEstacion)
                        dentroCoordenadas = false;
                    break;
            }
            eventType = xpp.next();
        }

        return estaciones;
    }

    public static List<Noticia> analizarRSS(File file) throws NullPointerException, XmlPullParserException, IOException {
        boolean dentroItem = false;
        boolean dentroTitle = false;
        boolean dentroLink = false;

        XmlPullParser xpp = Xml.newPullParser();
        xpp.setInput(new FileReader(file));
        int eventType = xpp.getEventType();
        ArrayList<Noticia> noticias = new ArrayList<>();
        Noticia tmpNoticia = null;

        while (eventType != XmlPullParser.END_DOCUMENT) {
            switch (eventType) {
                case XmlPullParser.START_TAG:
                    if (xpp.getName().equals("item")) {
                        dentroItem = true;
                        tmpNoticia = new Noticia();
                    }
                    if (xpp.getName().equals("title") && dentroItem) {
                        dentroTitle = true;
                        tmpNoticia.setTitle(xpp.getText());
                    }
                    if (xpp.getName().equals("link") && dentroItem) {
                        dentroLink = true;
                        tmpNoticia.setUrl(xpp.getText());
                    }
                    break;
                case XmlPullParser.TEXT:
                    if (dentroTitle)
                        tmpNoticia.setTitle(xpp.getText());
                    if (dentroLink)
                        tmpNoticia.setUrl(xpp.getText());
                    break;
                case XmlPullParser.END_TAG:
                    if (xpp.getName().equals("item")) {
                        dentroItem = false;
                        noticias.add(tmpNoticia);
                    }
                    if (xpp.getName().equals("title") && dentroItem)
                        dentroTitle = false;
                    if (xpp.getName().equals("link") && dentroItem)
                        dentroLink = false;
                    break;
            }
            eventType = xpp.next();
        }
        return noticias;
    }
    public static List<Tiempo> analizarTiempo(File file) throws NullPointerException, XmlPullParserException, IOException {
        boolean dentroPronostico_dias = false;
        boolean dentroDia = false;
        boolean dentroFecha = false;
        boolean dentroTemp_maxima = false;
        boolean dentroTemp_minima = false;
        boolean dentroIcon = false;
        boolean dentroTexto = false;

        XmlPullParser xpp = Xml.newPullParser();
        xpp.setInput(new FileReader(file));
        int eventType = xpp.getEventType();
        ArrayList<Tiempo> tiempos = new ArrayList<>();
        Tiempo tmpTiempo = null;

        while (eventType != XmlPullParser.END_DOCUMENT) {
            switch (eventType) {
                case XmlPullParser.START_TAG:
                    if (xpp.getName().equals("pronostico_dias")) {
                        dentroPronostico_dias = true;
                    }
                    if (xpp.getName().equals("dia") && dentroPronostico_dias) {
                        dentroDia = true;
                        tmpTiempo = new Tiempo();
                    }
                    if (xpp.getName().equals("fecha") && dentroDia) {
                        dentroFecha = true;
                    }
                    if (xpp.getName().equals("temp_maxima") && dentroDia) {
                        dentroTemp_maxima = true;
                    }
                    if (xpp.getName().equals("temp_minima") && dentroDia) {
                        dentroTemp_minima = true;
                    }
                    if (xpp.getName().equals("icono") && dentroDia) {
                        dentroIcon = true;
                    }
                    if (xpp.getName().equals("texto") && dentroDia) {
                        dentroTexto = true;
                    }
                    break;
                case XmlPullParser.TEXT:
                    if (dentroFecha)
                        tmpTiempo.setDate(xpp.getText());
                    if (dentroTemp_maxima)
                        tmpTiempo.setTempMax(xpp.getText());
                    if (dentroTemp_minima)
                        tmpTiempo.setTempMin(xpp.getText());
                    if (dentroIcon)
                        tmpTiempo.setIcon(xpp.getText());
                    if (dentroTexto)
                        tmpTiempo.setTexto(xpp.getText());
                    break;
                case XmlPullParser.END_TAG:
                    if (xpp.getName().equals("pronostico_dias")) {
                        dentroPronostico_dias = false;
                    }
                    if (xpp.getName().equals("dia") && dentroPronostico_dias) {
                        dentroDia = false;
                        tiempos.add(tmpTiempo);
                    }
                    if (xpp.getName().equals("fecha") && dentroDia) {
                        dentroFecha = false;
                    }
                    if (xpp.getName().equals("temp_maxima") && dentroDia) {
                        dentroTemp_maxima = false;
                    }
                    if (xpp.getName().equals("temp_minima") && dentroDia) {
                        dentroTemp_minima = false;
                    }
                    if (xpp.getName().equals("icono") && dentroDia) {
                        dentroIcon = false;
                    }
                    if (xpp.getName().equals("texto") && dentroDia) {
                        dentroTexto = false;
                    }
                    break;
            }
            eventType = xpp.next();
        }
        return tiempos;
    }

    /*public static ArrayList<Noticia> analizarNoticias(File file) throws XmlPullParserException, IOException {
        int eventType;
        ArrayList<Noticia> noticias = null;
        Noticia actual = null;
        boolean dentroItem = false;
        XmlPullParser xpp = Xml.newPullParser();
        xpp.setInput(new FileReader(file));
        eventType = xpp.getEventType();
        while (eventType != XmlPullParser.END_DOCUMENT) {
            switch (eventType) {
                case XmlPullParser.START_DOCUMENT:
                    noticias = new ArrayList<>();
                    break;
                case XmlPullParser.START_TAG:
                    if (xpp.getName().equals("item")) {
                        dentroItem = true;
                        actual = new Noticia();
                    } else if (xpp.getName().equals("title") && dentroItem) {
                        actual.setTitle(xpp.nextText());
                    } else if (xpp.getName().equals("link") && dentroItem) {
                        actual.setLink(xpp.getText());
                    } else if (xpp.getName().equals("description") && dentroItem) {
                        actual.setDescription(xpp.getText());
                    } else if (xpp.getName().equals("pubDate") && dentroItem) {
                        actual.setPubDate(xpp.getText());
                    }
                    break;
                case XmlPullParser.END_TAG:
                    if (xpp.getName().equals("item")) {
                        noticias.add(actual);
                        dentroItem = false;
                    }
                    break;
            }
            eventType = xpp.next();
        }
        //devolver el array de noticias
        return noticias;
    }

    public static void crearXML(ArrayList<Noticia> noticias, String fichero) throws IOException {
        FileOutputStream fout;
        fout = new FileOutputStream(new File(Environment.getExternalStorageDirectory().getAbsolutePath(), fichero));
        XmlSerializer serializer = Xml.newSerializer();
        serializer.setOutput(fout, "UTF-8");
        serializer.startDocument(null, true);
        serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true); //poner tabulación
        serializer.startTag(null, "titulares");
        for (int i = 0; i < noticias.size(); i++) {

            serializer.startTag(null, "item");
            serializer.startTag(null, "titulo");
            serializer.attribute(null, "fecha", noticias.get(i).getPubDate().toString());
            serializer.text(noticias.get(i).getTitle().toString());
            serializer.endTag(null, "titulo");
            serializer.startTag(null, "enlace");
            serializer.text(noticias.get(i).getLink().toString());
            serializer.endTag(null, "enlace");
            serializer.startTag(null, "descripcion");
            serializer.text(noticias.get(i).getDescription().toString());
            serializer.endTag(null, "descripcion");
            serializer.endTag(null, "item");
        }
        serializer.endTag(null, "titulares");
        serializer.endDocument();
        serializer.flush();
        fout.close();
    }*/
}
