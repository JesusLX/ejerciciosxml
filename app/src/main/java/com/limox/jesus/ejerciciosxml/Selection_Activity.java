package com.limox.jesus.ejerciciosxml;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Selection_Activity extends AppCompatActivity {

    Button btnPcWorld, btnElMundoTdy, btnElPais, btnLinuxMagacine;
    View.OnClickListener mClickListener;

    public static final String RSS_EMT = "http://www.elmundotoday.com/feed/";
    public static final String RSS_PW = "http://www.pcworld.com/index.rss";
    public static final String RSS_EP = "http://ep00.epimg.net/rss/elpais/portada.xml";
    public static final String RSS_LM = "http://www.linux-magazine.com/rss/feed/lmi_news";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selection);
        btnPcWorld = (Button) findViewById(R.id.s_btn1);
        btnElMundoTdy = (Button) findViewById(R.id.s_btn2);
        btnElPais = (Button) findViewById(R.id.s_btn3);
        btnLinuxMagacine = (Button) findViewById(R.id.s_btn4);

        mClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(Selection_Activity.this,NoticiasRss_Activity.class);
                switch (view.getId()) {
                    case R.id.s_btn1:
                        i.putExtra("rss", RSS_PW);
                        i.putExtra("title","Pc World");
                        startActivity(i);
                        break;
                    case R.id.s_btn2:
                        i.putExtra("rss", RSS_EMT);
                        i.putExtra("title","El mundo today");
                        startActivity(i);
                        break;
                    case R.id.s_btn3:
                        i.putExtra("rss", RSS_EP);
                        i.putExtra("title","El País");
                        startActivity(i);
                        break;
                    case R.id.s_btn4:
                        i.putExtra("rss", RSS_LM);
                        i.putExtra("title","Linux Magacine");
                        startActivity(i);
                        break;
                }
            }
        };
        btnPcWorld.setOnClickListener(mClickListener);
        btnElMundoTdy.setOnClickListener(mClickListener);
        btnElPais.setOnClickListener(mClickListener);
        btnLinuxMagacine.setOnClickListener(mClickListener);
    }
}
