package com.limox.jesus.ejerciciosxml.Adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.limox.jesus.ejerciciosxml.Pojo.Noticia;
import com.limox.jesus.ejerciciosxml.R;

import java.util.ArrayList;
import java.util.zip.Inflater;

/**
 * Created by jesus on 7/12/16.
 */

public class NoticiasAdapter extends ArrayAdapter<Noticia> {

    public NoticiasAdapter(Context context, ArrayList<Noticia> noticias) {
        super(context, R.layout.item_noticias, noticias);

    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        NewsHolder newsHolder = null;

        if (view == null){
            LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(getContext().LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.item_noticias,null);

            newsHolder = new NewsHolder();
            newsHolder.txvNoticias = (TextView) view.findViewById(R.id.in_txvUrl);

            view.setTag(newsHolder);
        }else
            newsHolder = (NewsHolder) view.getTag();

        newsHolder.txvNoticias.setText(getItem(position).getTitle());

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(getItem(position).getUrl()));
                getContext().startActivity(intent);
            }
        });

        return view;
    }
    class NewsHolder{
        TextView txvNoticias;
    }
}
