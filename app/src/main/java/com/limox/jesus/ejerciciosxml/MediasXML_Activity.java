package com.limox.jesus.ejerciciosxml;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;


public class MediasXML_Activity extends AppCompatActivity {

    TextView txvEdad;
    TextView txvSueldoMin;
    TextView txvSueldoMax;
    Button btnCalcular;

    String[] resultados;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medias_xml);

        txvEdad = (TextView) findViewById(R.id.mx_txvEdad);
        txvSueldoMin = (TextView) findViewById(R.id.mx_txvSuedloMin);
        txvSueldoMax = (TextView) findViewById(R.id.mx_txvSuedloMax);

        btnCalcular = (Button) findViewById(R.id.mx_btnCalcular);
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    resultados = Analisis.analizarNombres(MediasXML_Activity.this);
                    txvEdad.setText(resultados[0]);
                    txvSueldoMax.setText(resultados[1]);
                    txvSueldoMin.setText(resultados[2]);
                } catch (XmlPullParserException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
