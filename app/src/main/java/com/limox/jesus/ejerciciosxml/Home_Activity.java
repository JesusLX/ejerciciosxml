package com.limox.jesus.ejerciciosxml;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Home_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }

    public void onClick(View view){
        switch (view.getId()){
            case R.id.btnEjr1:
                startActivity(new Intent(Home_Activity.this,MediasXML_Activity.class));
                break;
            case R.id.btnEjr2:
                startActivity(new Intent(Home_Activity.this,Meteorogia_Activity.class));
                break;
            case R.id.btnEjr3:
                startActivity(new Intent(Home_Activity.this,Estaciones_Activity.class));
                break;
            case R.id.btnEjr4:
                startActivity(new Intent(Home_Activity.this,Selection_Activity.class));
                break;

        }


    }
}
